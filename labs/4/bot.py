import os
import telebot

bot = telebot.TeleBot(os.environ['TG_TOKEN'])

import requests
http_client = requests.Session()


import numpy as np
import tensorflow
from tensorflow.keras.models import load_model
from skimage.io import imread
from skimage.color import rgb2gray
from skimage.transform import resize
from skimage.util import img_as_ubyte


def prepare_image(file_id: str):
    path = bot.get_file(file_id).file_path
    data = imread(f'https://api.telegram.org/file/bot{bot.token}/{path}')
    gray_data = img_as_ubyte(rgb2gray(data))
    return resize(gray_data, (28, 28, 1), preserve_range=True)
    
@bot.message_handler(content_types=['photo'])
def photos_handler(message):
    image = prepare_image(message.photo[0].file_id)
    bot.reply_to(message, predict(image))
    
model = None
    
def get_model():
    global model
    if model is None:
        model = load_model('model.h5')
    return model
        

def predict(image):
    return get_model().predict(np.array([image])).argmax()

bot.infinity_polling()